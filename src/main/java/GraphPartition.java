import java.io.*;
import java.util.Scanner;
import java.util.Vector;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;

class Vertex implements Writable {
    public long id;                   // the vertex ID
    public Vector<Long> adjacent;     // the vertex neighbors
    public long centroid;             // the id of the centroid in which this vertex belongs to
    public short depth;               // the BFS depth
    public long adjacentVectorSize;

    Vertex(){}

    public Vertex(long idValue, Vector<Long> adjacentVector, long centroidValue, short depthValue){
        this.id = idValue;
        this.adjacent = adjacentVector;
        this.centroid = centroidValue;
        this.depth = depthValue;
        this.adjacentVectorSize = adjacentVector.size();
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeLong(id);
        dataOutput.writeLong(adjacentVectorSize);
        for (int i = 0; i< adjacentVectorSize; i++)
        {
            dataOutput.writeLong(adjacent.get(i));
        }
        dataOutput.writeLong(centroid);
        dataOutput.writeShort(depth);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        id = dataInput.readLong();
        adjacent= new Vector<>();
        adjacentVectorSize = dataInput.readLong();
        for (int i = 0; i< adjacentVectorSize; i++)
        {
            adjacent.add(dataInput.readLong());
        }
        centroid = dataInput.readLong();
        depth = dataInput.readShort();
    }

}

public class GraphPartition {
    static Vector<Long> centroids = new Vector<>();
    final static short max_depth = 8;
    static short BFS_depth = 0;

    public static class MyFirstMapper extends Mapper<Object, Text, LongWritable, Vertex> {
        static int count =0;
        @Override
        public void map(Object key, Text value, Context context)
                throws IOException, InterruptedException {
            Scanner s = new Scanner(value.toString()).useDelimiter(",");
            long id = s.nextLong();
            Vector<Long> adjacent = new Vector<>();
            while(s.hasNextLong()) {
                adjacent.add(s.nextLong());
            }
            short depth =0;
            long c= -1  ;
            if(count<10) {
                context.write(new LongWritable(id), new Vertex(id, adjacent, id, depth));
            } else {
                context.write(new LongWritable(id), new Vertex(id, adjacent, c, depth));
            }
            count+=1;
            s.close();
        }
    }

    public static class MySecondMapper extends Mapper<LongWritable, Vertex, LongWritable, Vertex> {
        @Override
        public void map(LongWritable id, Vertex value, Context context)
                throws IOException, InterruptedException {
            context.write( new LongWritable(value.id), value);
            if(value.centroid>0){
                for(Long n : value.adjacent){
                    Vector<Long> newVector = new Vector<>();
                    context.write(new LongWritable(n), new Vertex(n, newVector, value.centroid, BFS_depth));
                }
            }
        }
    }

    public static class MySecondReducer extends Reducer<LongWritable, Vertex, LongWritable, Vertex> {
        @Override
        public void reduce(LongWritable id, Iterable<Vertex> values, Context context)
                throws IOException, InterruptedException {
            short min_depth = 1000;
            short d = 0;
            long c = -1;
            Vector<Long> newVector = new Vector<>();
            for (Vertex v : values) {
                if(!v.adjacent.isEmpty()){
                    for(Long n : v.adjacent){
                        newVector.add(n);
                    }
                }
                if(v.centroid > 0 && v.depth < min_depth){
                    min_depth = v.depth;
                    c = v.centroid;
                }
            }
            Vertex m = new Vertex(id.get(),newVector,c,d);
            m.depth = min_depth;
            context.write(id, m);
        }
    }

    public static class MyThirdMapper extends Mapper<LongWritable, Vertex, LongWritable, LongWritable> {
        @Override
        public void map(LongWritable id, Vertex value, Context context)
                throws IOException, InterruptedException {
            context.write( new LongWritable(value.centroid), new LongWritable(1));
        }
    }

    public static class MyThirdReducer extends Reducer<LongWritable, LongWritable, LongWritable, LongWritable> {
        @Override
        public void reduce(LongWritable id, Iterable<LongWritable> values, Context context)
                throws IOException, InterruptedException {
            long m = 0;
            for (LongWritable v : values) {
                m+=v.get();
            }
            context.write(id, new LongWritable(m));
        }
    }

    public static void main ( String[] args ) throws Exception {
        Job job = Job.getInstance();
        job.setJobName("MyFirstJob");
        /* ... First Map-Reduce job to read the graph */
        job.setJarByClass(GraphPartition.class);
        job.setOutputKeyClass(LongWritable.class);
        job.setOutputValueClass(Vertex.class);
        job.setMapperClass(MyFirstMapper.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Vertex.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        FileInputFormat.setInputPaths(job,new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]+"/i0"));
        boolean success = job.waitForCompletion(true);
        if(success) {
            for ( short i = 0; i < max_depth; i++ ) {
                BFS_depth++;
                Job job2 = Job.getInstance();
                /* ... Second Map-Reduce job to do BFS */
                job2.setJobName("MySecondJob");
                job2.setJarByClass(GraphPartition.class);
                job2.setMapperClass(MySecondMapper.class);
                job2.setReducerClass(MySecondReducer.class);
                job2.setOutputKeyClass(LongWritable.class);
                job2.setOutputValueClass(Vertex.class);
                job2.setMapOutputKeyClass(LongWritable.class);
                job2.setMapOutputValueClass(Vertex.class);
                job2.setInputFormatClass(SequenceFileInputFormat.class);
                job2.setOutputFormatClass(SequenceFileOutputFormat.class);
                FileInputFormat.addInputPath(job2, new Path(args[1]+"/i"+i));
                FileOutputFormat.setOutputPath(job2, new Path(args[1]+"/i"+(i+1)));
                job2.waitForCompletion(true);
            }

        }

        /* ... Final Map-Reduce job to calculate the cluster sizes */
        Job job3 = Job.getInstance();
        /* ... Second Map-Reduce job to do BFS */
        job3.setJobName("MyThirdJob");
        job3.setJarByClass(GraphPartition.class);
        job3.setMapperClass(MyThirdMapper.class);
        job3.setReducerClass(MyThirdReducer.class);
        job3.setOutputKeyClass(LongWritable.class);
        job3.setOutputValueClass(LongWritable.class);
        job3.setMapOutputKeyClass(LongWritable.class);
        job3.setMapOutputValueClass(LongWritable.class);
        job3.setInputFormatClass(SequenceFileInputFormat.class);
        job3.setOutputFormatClass(TextOutputFormat.class);
        FileInputFormat.addInputPath(job3, new Path(args[1]+"/i8"));
        FileOutputFormat.setOutputPath(job3, new Path(args[2]));
        job3.waitForCompletion(true);
    }
}
