<h3>Graph Partitioning using Map-Reduce</h3>

Implemented a map-reduce program to partition a directed graph into k clusters in a breadth-first search fashion with multiple iterations.
